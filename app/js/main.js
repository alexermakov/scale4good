$(function () {

    $('.js_contact_page__question__list .question__item__top').click(function(e){
        e.preventDefault();
        $(this).closest('.question__item').find('.question__item__content').slideToggle(400,function(){
            $(this).closest('.question__item').toggleClass('active')
        });

    })


    $(".js__animate__left_1").each(function () {
        el = $(this)[0];
        delayTime = 0.15;
        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: -500,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__right_1").each(function () {
        el = $(this)[0];
        delayTime = 0.3;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: 500,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })





    $(".js__animate__left").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })

    $(".js__animate__right").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            x: 100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })

    $(".js__animate__top_0").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 100%'},
            y: 200,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__top_1").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:el,start: 'top 80%'},
            y: 200,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__scale").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ?  $(this).data('delay'): 0;

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing')[0],start: 'top 50%'},
            scale: 0,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__top_dot").each(function () {
        let el = $(this)[0];
        let delayTime = $(this).data('delay');

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing__text__list')[0],start: 'top 80%'},
            y: -50,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $(".js__animate__top_line").each(function () {
        let el = $(this)[0]
        let delayTime = $(this).data('delay');

        gsap.from(el, 1, {
            scrollTrigger:{trigger:$(el).closest('.modern__marketing__text__list')[0],start: 'top 80%'},
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {amount: 0.3}
        })
    })


    $.fn.isInViewport = function () {
        let elementTop = $(this).offset().top;
        let elementBottom = elementTop + $(this).outerHeight();

        let viewportTop = $(window).scrollTop();
        let viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };


    let animateStartCount = false;

    $(window).on('resize scroll', function () {
        if ($('.js_round__animate').isInViewport() && !animateStartCount) {

            $('.js_our_service__item__circle').each(function (index, element) {
                setTimeout(() => {
                    $(this).addClass('animate')
                }, 350*(index+1));

            });
        }

    });






});